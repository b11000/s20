


function msgToSelf(){
	console.log("Don't text her back.")
}

// While loop
let count = 10

while(count !== 0){
	msgToSelf();
	count--;
}

// display numbers from 1-5
let x = 1;

while(count !== 5){
	console.log(x);
	count++;
}

// Do-While Loop
// Do while loop is similar to the while loop. However, do while will allow us to run our loop at least once.

//While -> We check the condition first before running our codeblock
//do while -> it will do an instruction first before it will check the condition to run again

// Create a do-while loop which will be able to show the numbers in the console from 1-20 order

let counter = 1;

do{
	console.log(counter);
	counter++;
}while(counter<=20)

// For Loop
// This is an Infinite Loop
// for (let count = 0; count <=10; counter++){
// 	console.log(count)
// }

let fruits = ["Apple", "Mango", "Pineapple", "Guyabano"]
// show all the items in an array in the console using for loops:

for(count = 0; count < fruits.length; count++){
		console.log(fruits[count]);
}

let favoriteCountries = ["Philippines", "Japan", "U.K.", "Korea"]

for(let count = 0; count < favoriteCountries.length; count++)
	console.log(favoriteCountries[count])

// Global/local Scope

// Global scoped variables are variables that can be accessed inside a function or anywhere else in out script
let userName = 'super_knight_1000';

function sample(){
	// local scoped or function scoped variable
	// It cannot be accessed outside the function it was created from
	let heroName = "One punch Man"
	console.log(heroName)
	console.log(userName)
}

sample()

console.log(userName)
/*console.log(heroName)*/


//Strings are similar to Arrays

let powerpuffGirls = ['Blossom', 'Bubbles', 'Buttercup'];

console.log(powerpuffGirls[0])

let name = "Alexandro";

console.log(name[0])

console.log(name[name.length-1])

/*
Create a function able to receive a string as argument
	-Inside the function add a for loop
	-This for loop should be able to show/display all characters of word/strings.
	-Note: Strings are similar to Arrays
*/
function test(string){
	for(let x = 0; x < string.length; x++)
	console.log(string[x])
}
test("meow")