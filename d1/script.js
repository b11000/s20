// Introduction to JSON
// Javascript Object Notation
// a data format used by applications to store and transport data to one another.
// 
// 				JSON.stringify()
// 				Sending Data ==>
// CLIENT 				 				SERVER
// 				<== Receiving Data
// 					JSON.parse()
// 					
// Even though it has the JavaScript in its name, use of JSON is not limited to JavaScript-based applications and can also be used in other programming Languages.
// Files that store JSON data are saved with a file extension of .json

//JSON OBJECTS
//JSON is used for serializing different data types into bytes.
//Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
//byte =? binary digits (1 and 0) that is used to represent a chracter 

/*
JSON data format
Syntax
	{
		"propertyB": "valueB",
		"propertyB": "valueB",
	}

 */

/*
JS OBJECT
{
	city: "QC",
	province: "Metro Manila",
	country: 'Philippines'
}	

JSON
{
	'city: "QC",
	`province`: "Metro Manila",
	"country": "Philippines"
}

JSON ARRAY

"cities":[
	{
	'city: "QC",
	`province`: "Metro Manila",
	"country": "Philippines"
	},
	{
		'city: "QC",
		`province`: "Metro Manila",
		"country": "Philippines"
	},
	{
		'city: "QC",
		`province`: "Metro Manila",
		"country": "Philippines"
	}
]
*/

// JSON METHODS
// THE JSON contains methods for parsing and converting data into stringified JSON

/*
- Stringified JSON is a javascript object converted into a string to be used in other functions of a Javascript application

 */

let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]
console.log('Result from stringify method')
// The 'stringify' method is used to convert JS objects into JSON(string)
console.log(JSON.stringify(batchesArr))
// Object ==> JSON use (stringify)
let data = JSON.stringify({
	name: 'John',
	age: 31, 
	address: {
		city: 'Manila',
		country: 'Philippines'	
	} 
})

/*
Mini-Activity
Use the prompt method in order to gather user data to be supplied to the user details, then once the data gathered, convert it into stringify and print the details
User details:
firstName:
lastName:
age:
address: city, country, zip code
 */
let personDetails = [
	{
		firstName: 'Leonardo'
	},
	{
		lastName: 'Tomas'
	},
	{
		age: 31
	}
]

// let firstName = prompt("First Name: ")
// let lastName = prompt("Last Name: ")
// let age = prompt("How old are you?")
// let address = prompt("Where do you live")

// console.log(JSON.stringify(personDetails))
// let data1 = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age, 
// 	address: address
// })

// console.log(personDetails)


// userDetails = {
//     fname: prompt("First Name:", ''),
//     lName: prompt("Last Name:", ""),
//     age: prompt("Age:",""),
//     address: {
//         city: prompt("City:",),
//         country: prompt("Country:",""),
//         zip: prompt("Zip Code:",""),
//     }
// }
// 
// Converting Stringified JSON into js objects
// Information is commonly sent to application in stringified JSON and then converted back into objects.
// This happens bot for sending information to a backend app and sending information back to frontend app
// Upon receiving data, JSON tet can be converted to a JS object with parse
// 
let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}
]`

console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `
	{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines" 
		}
	}
`

console.log(JSON.parse(stringifiedObject))

